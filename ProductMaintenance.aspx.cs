﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Code behind for the ProductMaintenace page responsible for handling events.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// March 29, 2015 | Spring
/// </version>
public partial class ProductMaintenance :Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
    }
    /// <summary>
    /// Handles the RowUpdated event of the gvProducts control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewUpdatedEventArgs"/> instance containing the event data.</param>
    protected void gvProducts_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblError.Text = "A database issue occurred.<br/ ><br/ >" + "Error Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        } 
    }
}