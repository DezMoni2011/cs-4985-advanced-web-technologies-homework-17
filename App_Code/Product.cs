﻿using System;

/// <summary>
/// Summary description for Product
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// March 29, 2015 | Spring
/// </version>
public class Product
{

    /// <summary>
    /// The _product name
    /// </summary>
    private string _productName;
    /// <summary>
    /// The _units in stock
    /// </summary>
    private int _unitsInStock;
    /// <summary>
    /// The _product identifier
    /// </summary>
    private int _productId;

    /// <summary>
    /// Gets or sets the name of the product.
    /// </summary>
    /// <value>
    /// The name of the product.
    /// </value>
    /// <exception cref="ArgumentException">Product name msut not be null.</exception>
    public string ProductName 
    {
        get { return this._productName; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Product name msut not be null.");
            }

            this._productName = value;
        }
    }

    /// <summary>
    /// Gets or sets the units in stock.
    /// </summary>
    /// <value>
    /// The units in stock.
    /// </value>
    /// <exception cref="ArgumentException">Units in stock must be positive.</exception>
    public int UnitsInStock
    {
        get { return this._unitsInStock; }
        set
        {
            if (value < 0)
            {
                throw new ArgumentException("Units in stock must be positive.");
            }

            this._unitsInStock = value;
        }
    }

    /// <summary>
    /// Gets or sets the product identifier.
    /// </summary>
    /// <value>
    /// The product identifier.
    /// </value>
    /// <exception cref="ArgumentException">Product ID must be positive.</exception>
    public int ProductId
    {
        get { return this._productId; }
        set
        {
            if (value < 0)
            {
                throw new ArgumentException("Product ID must be positive.");
            }

            this._productId = value;
        }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Product"/> class.
    /// </summary>
	public Product()
	{
	}

    /// <summary>
    /// Initializes a new instance of the <see cref="Product"/> class.
    /// </summary>
    /// <param name="theProductId">The product identifier.</param>
    /// <param name="theProductName">Name of the product.</param>
    /// <param name="theUnitsInStock">The units in stock.</param>
    public Product(int theProductId, string theProductName, int theUnitsInStock)
    {
        this._productId = theProductId;
        this._productName = theProductName;
        this._unitsInStock = theUnitsInStock;
    }
}