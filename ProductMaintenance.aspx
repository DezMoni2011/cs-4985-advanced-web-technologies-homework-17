﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProductMaintenance.aspx.cs" Inherits="ProductMaintenance" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Chapter 17 : Northwind</title>
    <link href="Styles/Main.css" rel="stylesheet" />
</head>
<body>
    <header></header>

    <form id="mainForm" runat="server">
        <asp:GridView ID="gvProducts" runat="server" 
            AllowPaging="True" 
            AutoGenerateColumns="False" 
            BackColor="#DEBA84" 
            BorderColor="#DEBA84" 
            BorderStyle="None" 
            BorderWidth="1px" 
            CellPadding="3" 
            CellSpacing="2" 
            DataKeyNames="ProductID" 
            DataSourceID="odsProducts" OnRowUpdated="gvProducts_RowUpdated">
            <Columns>
                <asp:BoundField DataField="ProductID" HeaderText="ProductID" 
                    InsertVisible="False" ReadOnly="True" SortExpression="ProductID" />
                <asp:TemplateField HeaderText="ProductName" SortExpression="ProductName">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtProductName" runat="server" Text='<%# Bind("ProductName") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvProductName" runat="server" ControlToValidate="txtProductName" CssClass="error" Display="Dynamic" ErrorMessage="Please apply product name.">*</asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("ProductName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="UnitsInStock" SortExpression="UnitsInStock">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtUnitsInStock" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvUnitsInStock" runat="server" ControlToValidate="txtUnitsInStock" CssClass="error" Display="Dynamic" ErrorMessage="You must have a number of units in stock.">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cvUnitsInStock" runat="server" ControlToValidate="txtUnitsInStock" CssClass="error" Display="Dynamic" ErrorMessage="Must use a positive integer." Operator="GreaterThanEqual" Type="Integer" ValueToCompare="0">*</asp:CompareValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("UnitsInStock") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowEditButton="True" />
            </Columns>
            <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
            <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#FFF1D4" />
            <SortedAscendingHeaderStyle BackColor="#B95C30" />
            <SortedDescendingCellStyle BackColor="#F1E5CE" />
            <SortedDescendingHeaderStyle BackColor="#93451F" />
        </asp:GridView>
        <asp:SqlDataSource ID="sdsProductsTemporary" runat="server" 
            ConnectionString="<%$ ConnectionStrings:NorthwindConnectionSqlString %>" 
            ProviderName="<%$ ConnectionStrings:NorthwindConnectionSqlString.ProviderName %>" 
            SelectCommand="SELECT [ProductID], [ProductName], [UnitsInStock] FROM [tblProducts]" DeleteCommand="DELETE FROM [tblProducts] WHERE [ProductID] = ?" InsertCommand="INSERT INTO [tblProducts] ([ProductID], [ProductName], [UnitsInStock]) VALUES (?, ?, ?)" UpdateCommand="UPDATE [tblProducts] SET [ProductName] = ?, [UnitsInStock] = ? WHERE [ProductID] = ?">
            <DeleteParameters>
                <asp:Parameter Name="ProductID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="ProductID" Type="Int32" />
                <asp:Parameter Name="ProductName" Type="String" />
                <asp:Parameter Name="UnitsInStock" Type="Int16" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="ProductName" Type="String" />
                <asp:Parameter Name="UnitsInStock" Type="Int16" />
                <asp:Parameter Name="ProductID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:ObjectDataSource ID="odsProducts" runat="server"></asp:ObjectDataSource>

        <asp:Label ID="lblError" CssClass="error" runat="server"></asp:Label>

        <asp:ValidationSummary ID="vsEditProducts" CssClass="error" runat="server" HeaderText="Please correct the follwing errors." />
    </form>
</body>
</html>
